#include <string>
#include <utility>


#include "gmock/gmock.h"
#include "gtest/gtest-spi.h"
#include "gtest/gtest.h"

#pragma comment( lib, "gtestd.lib" )
#pragma comment( lib, "gmock.lib" )

#include <windows.h>
#include <tchar.h>
#include <stdio.h>

#define CreateFile CreateFileMock
HANDLE CreateFileMock(
		LPCSTR                  lpFileName,
		DWORD                   dwDesiredAccess,
		DWORD                   dwShareMode,
		LPSECURITY_ATTRIBUTES   lpSecurityAttributes,
		DWORD                   dwCreationDisposition,
		DWORD                   dwFlagsAndAttributes,
		HANDLE                  hTemplateFile
);
#define SetupComm SetupCommMock
BOOL SetupCommMock(
		HANDLE                  hFile,
		DWORD                   dwInQueue,
		DWORD                   dwOutQueue
);
#define SetCommState SetCommStateMock
BOOL SetCommStateMock(
		HANDLE                  hFile,
		LPDCB                   lpDCB
);
#define SetCommTimeouts SetCommTimeoutsMock
BOOL SetCommTimeoutsMock(
		HANDLE                  hFile,
		LPCOMMTIMEOUTS          lpCommTimeouts
);
#define PurgeComm PurgeCommMock
BOOL PurgeCommMock(
		HANDLE                  hFile,
		DWORD                   dwFlags
);
#define WriteFile WriteFileMock
BOOL WriteFileMock(
		HANDLE                  hFile,
		LPCVOID                 lpBuffer,
		DWORD                   nNumberOfBytesToWrite,
		LPDWORD                 lpNumberOfBytesWritten,
		LPOVERLAPPED            lpOverlapped
);
#define CloseHandle CloseHandleMock
BOOL CloseHandleMock(
		HANDLE                  hObject
);

void PrintCommState(DCB dcb);
int init_lsg_1050(HANDLE *hCom);
int open_usb_lsg_1050(HANDLE *hCom);
int close_usb_lsg_1050(HANDLE *hCom);
int write_line_lsg_1050(HANDLE *hCom,char *format, ... );
void read_line_lsg_1050(HANDLE *hCom);

#include "../src/controller.c"


using ::testing::_;
using ::testing::Mock;
using ::testing::Return;
using ::testing::StrEq;
using ::testing::Eq;
using ::testing::HasSubstr;
using ::testing::AnyNumber;

#if !defined(GTEST_CUSTOM_INIT_GOOGLE_TEST_FUNCTION_)
/************************/
/*!
 * @brief           Windows.h内のライブラリをモック
*/
/************************/
class MockWin{
	public:
		MockWin() {}

		MOCK_METHOD7(CreateFileMockA, HANDLE(
				LPCSTR                  lpFileName,
				DWORD                   dwDesiredAccess,
				DWORD                   dwShareMode,
				int                     lpSecurityAttributes,
				DWORD                   dwCreationDisposition,
				DWORD                   dwFlagsAndAttributes,
				HANDLE                  hTemplateFile
				)
		);
		MOCK_METHOD3(SetupCommMockA, BOOL(
				HANDLE                  hFile,
				DWORD                   dwInQueue,
				DWORD                   dwOutQueue
				)
		);
		MOCK_METHOD7(SetCommStateMockA, BOOL(
				HANDLE                  hFile,
				DWORD                   BaudRate,
				BYTE                    ByteSize,
				char                    EofChar,
				BYTE                    Parity,
				DWORD                   fParity,
				BYTE                    StopBits
				)
		);
		MOCK_METHOD6(SetCommTimeoutsMockA, BOOL(
				HANDLE                  hFile,
				DWORD					ReadIntervalTimeout,
				DWORD					ReadTotalTimeoutMultiplier,
				DWORD					ReadTotalTimeoutConstant,
				DWORD					WriteTotalTimeoutMultiplier,
				DWORD					WriteTotalTimeoutConstant
				)
		);
		MOCK_METHOD2(PurgeCommMockA, BOOL(
				HANDLE                  hFile,
				DWORD                   dwFlags
				)
		);
		MOCK_METHOD5(WriteFileMockA, BOOL(
				HANDLE                  hFile,
				LPCSTR                  lpBuffer,
				DWORD                   nNumberOfBytesToWrite,
				LPDWORD                 lpNumberOfBytesWritten,
				LPOVERLAPPED            lpOverlapped
				)
		);
		MOCK_METHOD1(CloseHandleMockA, BOOL(
				HANDLE hObject
				)
		);

	private:
		GTEST_DISALLOW_COPY_AND_ASSIGN_(MockWin);
};

MockWin* winLib;

HANDLE CreateFileMock(
		LPCSTR                  lpFileName,
		DWORD                   dwDesiredAccess,
		DWORD                   dwShareMode,
		LPSECURITY_ATTRIBUTES   lpSecurityAttributes,
		DWORD                   dwCreationDisposition,
		DWORD                   dwFlagsAndAttributes,
		HANDLE                  hTemplateFile
){
	return (HANDLE)winLib->CreateFileMockA(lpFileName, dwDesiredAccess, dwShareMode, 0, dwCreationDisposition, dwFlagsAndAttributes, hTemplateFile);
}

BOOL SetupCommMock(
		HANDLE                  hFile,
		DWORD                   dwInQueue,
		DWORD                   dwOutQueue
){
	return winLib->SetupCommMockA(hFile, dwInQueue, dwOutQueue);
}

BOOL SetCommStateMock(
		HANDLE                  hFile,
		LPDCB                   lpDCB
){
	return winLib->SetCommStateMockA(
		hFile,
		lpDCB->BaudRate,
		lpDCB->ByteSize,
		lpDCB->EofChar,
		lpDCB->Parity,
		lpDCB->fParity,
		lpDCB->StopBits
		);
}

BOOL SetCommTimeoutsMock(
		HANDLE                  hFile,
		LPCOMMTIMEOUTS          lpCommTimeouts
){
	return winLib->SetCommTimeoutsMockA(
		hFile,
		lpCommTimeouts->ReadIntervalTimeout,
		lpCommTimeouts->ReadTotalTimeoutMultiplier,
		lpCommTimeouts->ReadTotalTimeoutConstant,
		lpCommTimeouts->WriteTotalTimeoutMultiplier,
		lpCommTimeouts->WriteTotalTimeoutConstant
		);
}
BOOL PurgeCommMock(
		HANDLE                  hFile,
		DWORD                   dwFlags
){
	return winLib->PurgeCommMockA(hFile, dwFlags);
}

BOOL WriteFileMock(
	HANDLE                      hFile,
	LPCVOID                     lpBuffer,
	DWORD                       nNumberOfBytesToWrite,
	LPDWORD                     lpNumberOfBytesWritten,
	LPOVERLAPPED                lpOverlapped
){
	return winLib->WriteFileMockA(hFile, (LPCSTR)lpBuffer, nNumberOfBytesToWrite, lpNumberOfBytesWritten, lpOverlapped);
}

BOOL CloseHandleMock(
	HANDLE                      hObject
){
	return winLib->CloseHandleMockA(hObject);
}

/************************/
/*!
 * @brief           SetUp/TearDown for NormalTest
*/
/************************/
class NormalTest : public testing::Test {
	protected:
		HANDLE hCom;
		static void SetUpTestCase() {
			printf("[NormalTest]:SetUpTestCase\n");

		}
		static void TearDownTestCase() {
			printf("[NormalTest]:TearDownTestCase\n");
		}
		virtual void SetUp() { 
			winLib = new MockWin;
			HANDLE hCom;
			ON_CALL(*winLib, CreateFileMockA(_,_,_,_,_,_,_))
			.WillByDefault(Return(&hCom));
			ON_CALL(*winLib, SetupCommMockA(_,_,_))
			.WillByDefault(Return(TRUE));
			ON_CALL(*winLib, SetCommStateMockA(_,_,_,_,_,_,_))
			.WillByDefault(Return(TRUE));
			ON_CALL(*winLib, SetCommTimeoutsMockA(_,_,_,_,_,_))
			.WillByDefault(Return(TRUE));
			ON_CALL(*winLib, PurgeCommMockA(_,_))
			.WillByDefault(Return(TRUE));
			ON_CALL(*winLib, WriteFileMockA(_,_,_,_,_))
			.WillByDefault(Return(TRUE));
			ON_CALL(*winLib, CloseHandleMockA(_))
			.WillByDefault(Return(TRUE));

			EXPECT_CALL(*winLib, CreateFileMockA(_,_,_,_,_,_,_))
			.Times(AnyNumber());
			EXPECT_CALL(*winLib, SetupCommMockA(_,_,_))
			.Times(AnyNumber());
			EXPECT_CALL(*winLib, SetCommStateMockA(_,_,_,_,_,_,_))
			.Times(AnyNumber());
			EXPECT_CALL(*winLib, SetCommTimeoutsMockA(_,_,_,_,_,_))
			.Times(AnyNumber());
			EXPECT_CALL(*winLib, PurgeCommMockA(_,_))
			.Times(AnyNumber());
			EXPECT_CALL(*winLib, WriteFileMockA(_,_,_,_,_))
			.Times(AnyNumber());
			EXPECT_CALL(*winLib, CloseHandleMockA(_))
			.Times(AnyNumber());

		}
		virtual void TearDown(){
			EXPECT_TRUE(Mock::VerifyAndClearExpectations(winLib));
			delete winLib;
		}
};

/************************/
/*!
 * @brief           Normal Test (CreateFile is Colled)
 *                  Test for open_usb_lsg_1050
*/
/************************/
TEST_F(NormalTest, 01_OpenUsb_CreateFile) {
	EXPECT_CALL(*winLib, CreateFileMockA(StrEq("COM4"),GENERIC_READ | GENERIC_WRITE,0,NULL,OPEN_EXISTING,0,NULL))
	.Times(1);

	EXPECT_EQ(0, open_usb_lsg_1050(&hCom));
}
/************************/
/*!
 * @brief           Normal Test (SetupComm is Colled)
 *                  Test for open_usb_lsg_1050
*/
/************************/
TEST_F(NormalTest, 02_OpenUsb_SetupComm) {
	EXPECT_CALL(*winLib, SetupCommMockA(_,2048,2048))
	.Times(1)
	.WillOnce(Return(TRUE));

	EXPECT_EQ(0, open_usb_lsg_1050(&hCom));
}
/************************/
/*!
 * @brief           Normal Test (SetCommStat is Colled)
 *                  Test for open_usb_lsg_1050
*/
/************************/
TEST_F(NormalTest, 03_OpenUsb_SetCommState) {
	EXPECT_CALL(*winLib, SetCommStateMockA(_,CBR_9600, 8, 0xa, NOPARITY, FALSE, ONESTOPBIT))
	.Times(1)
	.WillOnce(Return(TRUE));

	EXPECT_EQ(0, open_usb_lsg_1050(&hCom));
}
/************************/
/*!
 * @brief           Normal Test (SetCommTimeouts is Colled)
 *                  Test for open_usb_lsg_1050
*/
/************************/
TEST_F(NormalTest, 04_OpenUsb_SetCommTimeouts) {
	EXPECT_CALL(*winLib, SetCommTimeoutsMockA(_,50,0,100,50,10))
	.Times(1)
	.WillOnce(Return(TRUE));

	EXPECT_EQ(0, open_usb_lsg_1050(&hCom));
}
/************************/
/*!
 * @brief           Normal Test (PurgeComm is Colled)
 *                  Test for write_line_lsg_1050
*/
/************************/
TEST_F(NormalTest, 05_WriteLine_PurgeComm) {
	EXPECT_CALL(*winLib, PurgeCommMockA(_,PURGE_RXABORT | PURGE_RXCLEAR | PURGE_TXABORT | PURGE_TXCLEAR))
	.Times(1)
	.WillOnce(Return(TRUE));

	EXPECT_EQ(0, write_line_lsg_1050(&hCom, ":MODE CC"));
}
/************************/
/*!
 * @brief           Normal Test (WriteFile is Colled)
 *                  Test for write_line_lsg_1050
*/
/************************/
TEST_F(NormalTest, 06_WriteLine_WriteFile) {
//	EXPECT_CALL(*winLib, WriteFileMockA(_,_,_,_,_))
	EXPECT_CALL(*winLib, WriteFileMockA(_,StrEq(":MODE CC\n"),9,_,_))
	.Times(1)
	.WillOnce(Return(TRUE));

	EXPECT_EQ(0, write_line_lsg_1050(&hCom, ":MODE CC"));
}
/************************/
/*!
 * @brief           Normal Test (WriteFile is Colled)
 *                  Test for init_lsg_1050
*/
/************************/
TEST_F(NormalTest, 07_Init_WriteFile) {
	EXPECT_CALL(*winLib, WriteFileMockA(_,HasSubstr("*rst;*cls"),_,_,_))
	.Times(1)
	.WillOnce(Return(TRUE));

	EXPECT_EQ(0, init_lsg_1050(&hCom));
}
/************************/
/*!
 * @brief           Normal Test (WriteFile is Colled)
 *                  Test for close_usb_lsg_1050
*/
/************************/
TEST_F(NormalTest, 08_CloseUsb_WriteFile) {
	EXPECT_CALL(*winLib, WriteFileMockA(_,HasSubstr(":UTIL:REM OFF"),_,_,_))
	.Times(1)
	.WillOnce(Return(TRUE));

	EXPECT_EQ(0, close_usb_lsg_1050(&hCom));
}

/************************/
/*!
 * @brief           SetUp/TearDown for SemiNormalTest
*/
/************************/
class SemiNormalTest : public testing::Test {
	protected:
		HANDLE hCom;
		static void SetUpTestCase() {
			printf("[SemiNormalTest]:SetUpTestCase\n");
		}
		static void TearDownTestCase() {
			printf("[SemiNormalTest]:TearDownTestCase\n");
		}
		virtual void SetUp() { 
			winLib = new MockWin;
			ON_CALL(*winLib, SetupCommMockA(_,_,_))
			.WillByDefault(Return(TRUE));
			ON_CALL(*winLib, SetCommStateMockA(_,_,_,_,_,_,_))
			.WillByDefault(Return(TRUE));
			ON_CALL(*winLib, SetCommTimeoutsMockA(_,_,_,_,_,_))
			.WillByDefault(Return(TRUE));
			ON_CALL(*winLib, PurgeCommMockA(_,_))
			.WillByDefault(Return(TRUE));
			
			EXPECT_CALL(*winLib, CreateFileMockA(_,_,_,_,_,_,_))
			.Times(AnyNumber());
			EXPECT_CALL(*winLib, SetupCommMockA(_,_,_))
			.Times(AnyNumber());
			EXPECT_CALL(*winLib, SetCommStateMockA(_,_,_,_,_,_,_))
			.Times(AnyNumber());
			EXPECT_CALL(*winLib, SetCommTimeoutsMockA(_,_,_,_,_,_))
			.Times(AnyNumber());
			EXPECT_CALL(*winLib, PurgeCommMockA(_,_))
			.Times(AnyNumber());
		}
		virtual void TearDown(){
			printf("[SemiNormalTest]:TearDown\n");
			EXPECT_TRUE(Mock::VerifyAndClearExpectations(winLib));
			delete winLib;
		}
};
/************************/
/*!
 * @brief           Semi-Normal Test (CreateFile failed)
 *                  Test for open_usb_lsg_1050
*/
/************************/
TEST_F(SemiNormalTest, 01_OpenUsb_CreateFileFailed) {
	EXPECT_CALL(*winLib, CreateFileMockA(_,_,_,_,_,_,_))
	.Times(1)
	.WillOnce(Return(INVALID_HANDLE_VALUE));

	EXPECT_EQ(1, open_usb_lsg_1050(&hCom));

}
/************************/
/*!
 * @brief           Semi-Normal Test (SetupComm failed)
 *                  Test for open_usb_lsg_1050
*/
/************************/
TEST_F(SemiNormalTest, 02_OpenUsb_SetupCommFailed) {
	EXPECT_CALL(*winLib, SetupCommMockA(_,_,_))
	.Times(1)
	.WillOnce(Return(FALSE));

	EXPECT_EQ(2, open_usb_lsg_1050(&hCom));

}
/************************/
/*!
 * @brief           Semi-Normal Test (SetCommState failed)
 *                  Test for open_usb_lsg_1050
*/
/************************/
TEST_F(SemiNormalTest, 03_OpenUsb_SetCommStateFailed) {
	EXPECT_CALL(*winLib, SetCommStateMockA(_,_,_,_,_,_,_))
	.Times(1)
	.WillOnce(Return(FALSE));


	EXPECT_EQ(3, open_usb_lsg_1050(&hCom));

}
/************************/
/*!
 * @brief           Semi-Normal Test (SetCommTimeouts failed)
 *                  Test for open_usb_lsg_1050
*/
/************************/
TEST_F(SemiNormalTest, 04_OpenUsb_SetCommTimeoutsFailed) {
	EXPECT_CALL(*winLib, SetCommTimeoutsMockA(_,_,_,_,_,_))
	.Times(1)
	.WillOnce(Return(FALSE));

	EXPECT_EQ(4, open_usb_lsg_1050(&hCom));

}
/************************/
/*!
 * @brief           Semi-Normal Test (PurgeComm failed)
 *                  Test for write_line_lsg_1050
*/
/************************/
TEST_F(SemiNormalTest, 05_WriteLine_PurgeCommFailed) {
	EXPECT_CALL(*winLib, PurgeCommMockA(_,_))
	.Times(1)
	.WillOnce(Return(FALSE));

	EXPECT_EQ(1, write_line_lsg_1050(&hCom, ":MODE CC"));

}
/************************/
/*!
 * @brief           Semi-Normal Test (WriteFile failed)
 *                  Test for write_line_lsg_1050
*/
/************************/
TEST_F(SemiNormalTest, 06_WriteLine_WriteFileFailed) {
	EXPECT_CALL(*winLib, WriteFileMockA(_,_,_,_,_))
	.Times(1)
	.WillOnce(Return(FALSE));

	EXPECT_EQ(2, write_line_lsg_1050(&hCom, ":MODE CC"));

}
/************************/
/*!
 * @brief           Semi-Normal Test (PurgeComm failed)
 *                  Test for init_lsg_1050
*/
/************************/
TEST_F(SemiNormalTest, 07_Init_PurgeCommFailed) {
	EXPECT_CALL(*winLib, PurgeCommMockA(_,_))
	.Times(1)
	.WillOnce(Return(FALSE));

	EXPECT_EQ(1, init_lsg_1050(&hCom));
}
/************************/
/*!
 * @brief           Semi-Normal Test (WriteFile failed)
 *                  Test for init_lsg_1050
*/
/************************/
TEST_F(SemiNormalTest, 08_Init_WriteFileFailed) {
	EXPECT_CALL(*winLib, WriteFileMockA(_,_,_,_,_))
	.Times(1)
	.WillOnce(Return(FALSE));

	EXPECT_EQ(2, init_lsg_1050(&hCom));
}
/************************/
/*!
 * @brief           Semi-Normal Test (PurgeComm failed)
 *                  Test for close_usb_lsg_1050
*/
/************************/
TEST_F(SemiNormalTest, 09_CloseUsb_PurgeCommFailed) {
	EXPECT_CALL(*winLib, PurgeCommMockA(_,_))
	.Times(1)
	.WillOnce(Return(FALSE));

	EXPECT_EQ(1, close_usb_lsg_1050(&hCom));

}
/************************/
/*!
 * @brief           Semi-Normal Test (WriteFile failed)
 *                  Test for close_usb_lsg_1050
*/
/************************/
TEST_F(SemiNormalTest, 10_CloseUsb_WriteFileFailed) {
	EXPECT_CALL(*winLib, WriteFileMockA(_,_,_,_,_))
	.Times(1)
	.WillOnce(Return(FALSE));

	EXPECT_EQ(2, close_usb_lsg_1050(&hCom));

}
int main(int argc, char** argv) {
	std::cout << "Running main() from gmock_main.cc\n";
	// Since Google Mock depends on Google Test, InitGoogleMock() is
	// also responsible for initializing Google Test.  Therefore there's
	// no need for calling testing::InitGoogleTest() separately.
	testing::InitGoogleMock(&argc, argv);
	return RUN_ALL_TESTS();
}

#endif  // !defined(GTEST_CUSTOM_INIT_GOOGLE_TEST_FUNCTION_)