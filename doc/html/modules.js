var modules =
[
    [ "USB接続を確立(open_usb_lsg_1050)", "group__open__usb__group.html", "group__open__usb__group" ],
    [ "初期設定(init_lsg_1050)", "group__init__group.html", "group__init__group" ],
    [ "USB接続を切断(close_usb_lsg_1050)", "group__close__usb__group.html", "group__close__usb__group" ],
    [ "コマンドを送信(write_line_lsg_1050)", "group__write__line__group.html", "group__write__line__group" ],
    [ "データを受信(read_line_lsg_1050)", "group__read__line__group.html", "group__read__line__group" ]
];