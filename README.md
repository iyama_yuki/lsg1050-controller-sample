## lsg1050-controller-sample
lsg1050-controller-sampleは、教育用のプロジェクトです。このプロジェクトでは、電子負荷装置（LSG 1050）を制御するプログラムの作成を通して、計測器を制御するプログラムの作り方を学びます。

## 環境
* Windows 10 64bit
* VSCode 1.39.0
* WSL

## ファイル構成
```
lsg1050-controller-sample/
      ┗ doc/
          ┗ html/         ...   Doxygenで生成したドキュメント
          ┗ Doxyfile      ...   Doxygenの構成ファイル
      ┗ src/
          ┗ main.c        ...   controller.cを起動するプログラム
          ┗ controller.c  ...   作成するプログラム
      ┗ test/
          ┗ test.cc       ...   controller.cをテストするプログラム
``` 
## LSG 1050とは
[LSG 1050][link-lsg1050]は、TEXIO社製の電子負荷装置です。USB/RS-232C/GP-IBのインタフェースに対応しておりプログラムから制御することが可能です。

[link-lsg1050]:https://www.texio.co.jp/product/detail/159

### LSG 1050の制御方法
ここでは、USBでの制御を想定してプログラムを作成します。LSG 1050はUSB CDCプロトコルで通信する仕様となっており、Windows環境に標準で入っている仮想COMポートを生成するドライバ([usbser.sys][link-usbser])で制御する事ができます。

[link-usbser]:https://docs.microsoft.com/ja-jp/windows-hardware/drivers/usbcon/usb-driver-installation-based-on-compatible-ids

### usbser.sysのAPI(Apprication Programming Interface)
**usbser.sys**ドライバは、Windows標準のAPI(Apprication Programming Interface)を用いることで、ユーザが作成したアプリケーションと通信することが出来ます。具体的には、指定したCOMポートを開く為のコマンド(CreateFile)や、デバイスにデータを送るコマンド(WriteFile)等が用意されており、これを用いてユーザアプリケーションから制御します。

## USB制御の流れ
実際のUSB制御の流れは以下の様になります。

```mermaid
sequenceDiagram;

participant A as ユーザーアプリ
participant B as usbser.sys
participant C as LSG 1050ファームウェア

A->>B:ポートのOpenリクエスト
  alt ポートのOpenに成功
    B -->> A:ハンドル生成(hCom)
  else ポートのOpenに失敗
    B -->> A:INVALID_HANDLE_VALUE
  end

A->>B:通信の初期設定
  alt 設定に成功
    B -->> A:０
  else 設定に失敗
    B -->> A:０以外
  end

A->>B:コマンド送信リクエスト
B->> C:LSG 1050にコマンド送信
  alt 通信に成功
   C -->> B:成功
   B -->> A: ０    
  else 通信に失敗
    C -->> B: エラー
    B -->> A: ０以外
  end

A->>B:ハンドルのクローズ
```

