#include <windows.h>
#include <tchar.h>
#include <stdio.h>

void PrintCommState(DCB dcb);
int init_lsg_1050(HANDLE *hCom);
int open_usb_lsg_1050(HANDLE *hCom);
int close_usb_lsg_1050(HANDLE *hCom);
int write_line_lsg_1050(HANDLE *hCom,char *format, ... );
void read_line_lsg_1050(HANDLE *hCom);
#define Print printf
/************************/
/*!
 * @brief           メイン関数
 * @return          None
*/
/************************/
int main (void){
	int result;
	HANDLE hCom;
	result = open_usb_lsg_1050(&hCom);
	result = init_lsg_1050(&hCom);
	if(result != 0){
		Print("init_lsg_1050 failed.\n");
	}
	result = write_line_lsg_1050(&hCom,":MODE CC");
	if(result != 0){
		Print("write_line_lsg_1050 failed.\n");
	}
	result = write_line_lsg_1050(&hCom,":INP ON");
	if(result != 0){
		Print("write_line_lsg_1050 failed.\n");
	}
	result = close_usb_lsg_1050(&hCom);
	if(result != 0){
		Print("close_usb_lsg_1050 failed.\n");
	}

	return 0;
}