/************************/
/*! @file       controller.c
	@brief      電子負荷(LSG 1050)を制御するコントローラ
	@version    0.1
	@author     Tomoyuki Sugiyama
	@date       2019/09/27
*/
/************************/
/**
 * @mainpage Revision Recode
 *          <td colspan="2">
 *              <table border="3" align="center">
 *              <tr>
 *              <th width="100">Revision</th>   <th width="450">Detail</th> <th width="150">Date</th>   <th width="150">Writer</th>
 *              </tr>
 *              <tr align="center"><th> 1.0 </th>   <td> 新規作成 </td> <td> 2019/10/04 </td>   <td> T.SUGIYAMA </td></tr>
 *              </table>
 *          </td>
 */


 /*!
 * @defgroup        open_usb_group              USB接続を確立(open_usb_lsg_1050)
 * @defgroup        init_group                  初期設定(init_lsg_1050)
 * @defgroup        close_usb_group             USB接続を切断(close_usb_lsg_1050)
 * @defgroup        write_line_group            コマンドを送信(write_line_lsg_1050)
 * @defgroup        read_line_group             データを受信(read_line_lsg_1050)
 */

#include <windows.h>
#include <tchar.h>
#include <stdio.h>

#define Print printf
#define DEBUG_LSG_1050
/************************/
/*!
 * @brief           シリアル通信設定をコンソールに出力
 * @param [in]      dcb     Control setting for a serial communications device
*/
/************************/
void PrintCommState(DCB dcb){
	Print("BoudRate = %d, ByteSize = %d. Parity = %d, StopBits = %d EofChar = 0x%x\n",dcb.BaudRate, dcb.ByteSize, dcb.Parity, dcb.StopBits,dcb.EofChar);
}
/************************/
/*!
 * @brief           電子負荷(LSG 1050)とのUSB接続を確立後、
 *                   シリアル通信設定、受信タイムアウト時間を設定
 * @param [in]      hCom        A handle to the communications resource.
 * @return          0           正常終了<br>
 *                  1           CreateFile異常終了<br>
 *                  2           SetupComm異常終了<br>
 *                  3           SetCommState異常終了<br>
 *                  4           SetCommTimeouts異常終了<br>
 *                  5           PurgeComm異常終了<br>
 *                  99          GetCommState異常終了
 * @ingroup         open_usb_group
*/
/************************/
int open_usb_lsg_1050(HANDLE *hCom){
// open_usb_lsg_1050は、電子負荷(LSG 1050)とのUSB接続と、各種通信設定を行う関数です。
// 下記URLのサンプルプログラムを参考に作成します。
// https://docs.microsoft.com/ja-jp/windows/win32/devio/configuring-a-communications-resource
//
// open_usb_lsg_1050内に追加する関数の仕様
// CreateFile:		指定したCOMポートのためのハンドル(hCom)を開きます。
//					設定するCOMポートは、
//					”COM4”とします。
// 					以降のコマンドでは、開いたハンドル(hCom)を指定します。
// SetupComm:		送受信バッファの初期化を行います。
//					サンプルにはありませんので、下記URLを参考に作成します。
//					https://docs.microsoft.com/ja-jp/windows/win32/api/winbase/nf-winbase-setupcomm
//					設定内容は、
//					dwInQueue:2048 dwOutQueue:2048
//					とします。
// SetCommState:	シリアル通信設定を行います。
//					設定内容は、
//					BaudRate:9600 ByteSize:8 Parity:NOPARITY StopBits ONESTOPBIT
//					EofChar:0xa fParity:FALSE
//					とします。
// SetCommTimeouts:	送受信のタイムアウト設定を行います。
//					サンプルにはありませんので、下記URLを参考に作成します。
//					https://docs.microsoft.com/ja-jp/windows/win32/api/winbase/nf-winbase-setcommtimeouts
//					設定内容は、
//					ReadIntervalTimeout:50 ReadTotalTimeoutMultiplier:0
//					ReadTotalTimeoutConstant:100 WriteTotalTimeoutConstant:10
//					WriteTotalTimeoutMultiplier:50
//					とします。
//
// ここから下にプログラムを作成します。
// 注：サンプルのhComは*hComに置き換えてください。

	return (0);
}
/************************/
/*!
 * @brief           電子負荷(LSG 1050)にコマンドを送信
 * @param [in]      hCom        A handle to the communications resource.
 * @param [in]      format      出力コマンド
 * @param [in]      ...         コマンドに設定する引数
 * @return          0           正常終了<br>
 *                  1           PurgeComm異常終了<br>
 *                  2           WriteFile異常終了
 * @ingroup         write_line_group
*/
/************************/
int write_line_lsg_1050(HANDLE *hCom,char *format, ... ){
// write_line_lsg_1050は、電子負荷(LSG 1050)にコマンドを送信する関数です。
//
// write_line_lsg_1050内に追加する関数の仕様
// PurgeComm:		送受信バッファの中身をクリアします。
//					下記URLを参考に作成します。
//					https://docs.microsoft.com/ja-jp/windows/win32/api/winbase/nf-winbase-purgecomm
//					設定内容は、
//					dwFlags:PURGE_RXABORT | PURGE_RXCLEAR | PURGE_TXABORT | PURGE_TXCLEAR
//					とします。
// WriteFile:		電子負荷(LSG 1050)にコマンドを送信します。
//					下記URLを参考に作成します。
//					https://docs.microsoft.com/ja-jp/windows/win32/api/fileapi/nf-fileapi-readfile
//					設定内容は、
//					lpBuffer:message nNumberOfBytesToRead:messageの文字列の長さ+1
//					lpNumberOfBytesRead:&dwBytesWrite lpOverlapped:NULL 
//					とします。
	va_list va;
	BOOL fSuccess;
	DWORD dwBytesWrite;
	char message[256];
	int messageLenght = 0;

// 以下は可変引数に対する処理です。
	va_start(va, format);
	vsprintf((char*)message, format, va);
	va_end(va);
// ここから下にプログラムを作成します。

	return (0);
}
/************************/
/*!
 * @brief           電子負荷(LSG 1050)に初期設定値を送信
 * @param [in]      hCom        A handle to the communications resource.
 * @return          0           正常終了<br>
 *                  0以外     異常終了
 * @ingroup         init_group
*/
/************************/
int init_lsg_1050(HANDLE *hCom){
// init_lsg_1050は、電子負荷(LSG 1050)を初期化する関数です。
//
// init_lsg_1050の仕様
//					"*rst;*cls"コマンドを電子負荷(LSG 1050)に送信します。
// ここから下にプログラムを作成します。

	return (0);
}
/************************/
/*!
 * @brief           電子負荷(LSG 1050)の出力、リモートをOFFに変更後、
 *                  USB接続を切断
 * @param [in]      hCom        A handle to the communications resource.
 * @return          0           正常終了<br>
 *                  0以外     異常終了
 * @ingroup         close_usb_group
*/
/************************/
int close_usb_lsg_1050(HANDLE *hCom){
// close_usb_lsg_1050は、電子負荷(LSG 1050)を初期化する関数です。
//
// close_usb_lsg_1050の仕様
//					":INP OFF;:UTIL:REM OFF"コマンドを電子負荷(LSG 1050)に送信します。
// CloseHandle:		指定したハンドル(hCom)を閉じます。
//					下記URLを参考に作成します。
//					https://docs.microsoft.com/en-us/windows/win32/api/handleapi/nf-handleapi-closehandle
// ここから下にプログラムを作成します。

	return (0);
}

/************************/
/*!
 * @brief           電子負荷(LSG 1050)からデータを受信
 * @param [in]      hCom        A handle to the communications resource.
 * @ingroup         read_line_group
*/
/************************/
void read_line_lsg_1050(HANDLE *hCom){

}
