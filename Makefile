BUILDTOOLS = C:/Program Files (x86)/Microsoft Visual Studio/2019/BuildTools/

VCINSTALLDIR    = $(BUILDTOOLS)VC/Tools/MSVC/14.23.28105/

WindowsSDKDir   = C:/Program Files (x86)/Windows Kits/10/

VCTools         = $(VCINSTALLDIR)Bin/Hostx64/x64

VCLibraries     = $(VCINSTALLDIR)lib/x64

VCIncludes      = $(VCINSTALLDIR)/include

OSLibrariesUCRT     = $(WindowsSDKDir)Lib/10.0.18362.0/ucrt/x64
OSLibrariesUCRTEnclave     = $(WindowsSDKDir)Lib/10.0.18362.0/ucrt_enclave/x64
OSLibrariesUM     = $(WindowsSDKDir)Lib/10.0.18362.0/um/x64

OSIncludesUCRT      = $(WindowsSDKDir)Include/10.0.18362.0/ucrt
OSIncludesUM      = $(WindowsSDKDir)Include/10.0.18362.0/um
OSIncludesShared      = $(WindowsSDKDir)Include/10.0.18362.0/shared

# Google Test root directory
GTEST_ROOT = lib/googletest-release-1.8.1/googletest/
GTEST_INCLUDE = $(GTEST_ROOT)/include/
GTEST_LIB = lib/gtest/
GTEST_SRC = $(GTEST_ROOT)/src/gtest-all.cc

GMOCK_ROOT = lib/googletest-release-1.8.1/googlemock/
GMOCK_INCLUDE = $(GMOCK_ROOT)/include/
GMOCK_LIB = lib/gmock/
GMOCK_SRC = $(GMOCK_ROOT)/src/gmock-all.cc

BIN                 = bin/
TARGET              = bin/usb-cdc-controller.exe

SRC_MAIN            = src/main.c
SRC                 = src/controller.c
SRC_TEST            = test/test.cc

CC                  = cl.exe

gtest-download:
	@if [ ! -d lib ]; \
		then echo "mkdir -p lib"; mkdir -p lib; \
		git clone https://github.com/google/googletest.git -b release-1.8.1 lib/googletest-release-1.8.1; \
	fi


gtest-all.obj:
	@if [ ! -d $(GTEST_LIB) ]; \
		then echo "mkdir -p $(GTEST_LIB)"; mkdir -p $(GTEST_LIB); \
		$(CC) /Fo"$(GTEST_LIB)" /Fd"$(GTEST_LIB)/gtest.pdb" /c /EHsc /GS /W3 /Zc:wchar_t  /Zi /Gm- /Od /Zc:inline /fp:precise /D "WIN32" /D "_VARIADIC_MAX=10" /D "_DEBUG" /D "_LIB" /D "_MBCS" /WX- /Zc:forScope /RTC1 /Gd /MTd /FC /nologo /diagnostics:column /TP /I"$(VCIncludes)"  /I"$(OSIncludesUCRT)" /I"$(OSIncludesUM)" /I"$(OSIncludesShared)"  /I"$(GTEST_INCLUDE)" /I"$(GTEST_ROOT)" $(GTEST_SRC); \
	fi

gmock-all.obj:
	@if [ ! -d $(GMOCK_LIB) ]; \
		then echo "mkdir -p $(GMOCK_LIB)"; mkdir -p $(GMOCK_LIB); \
		$(CC) /Fo"$(GMOCK_LIB)"  /Fd"$(GMOCK_LIB)/gmock.pdb" /c /EHsc  /GS /W3 /Zc:wchar_t  /Zi /Gm-  /Od /Zc:inline /fp:precise /D "WIN32" /D "_VARIADIC_MAX=10" /D "_DEBUG" /D "_LIB" /D "_UNICODE" /D "UNICODE" /WX- /Zc:forScope  /RTC1 /Gd /MTd /FC /nologo   /diagnostics:column  /TP /I"$(VCIncludes)"  /I"$(OSIncludesUCRT)" /I"$(OSIncludesUM)" /I"$(OSIncludesShared)"  /I"$(GMOCK_INCLUDE)" /I"$(GMOCK_ROOT)" /I"$(GTEST_INCLUDE)" /I"$(GTEST_ROOT)" $(GMOCK_SRC); \
	fi

setup: gtest-download gtest-all.obj gmock-all.obj
	lib.exe /OUT:"$(GTEST_LIB)/gtestd.lib" /MACHINE:X64 $(GTEST_LIB)/gtest-all.obj
	lib.exe /OUT:"$(GMOCK_LIB)/gmock.lib"  /MACHINE:X64 $(GMOCK_LIB)/gmock-all.obj

build:  $(SRC_MAIN) $(SRC)
	@if [ ! -d $(BIN) ]; \
      then echo "mkdir -p $(BIN)"; mkdir -p $(BIN); \
    fi
	$(CC) /Fe"$(TARGET)" /Fo"$(BIN)" /Fd"$(BIN)" /ZI /D "WIN32" /D "_DEBUG"  /D "_CONSOLE" /EHsc /MTd /I"$(VCIncludes)" /I"$(OSIncludesUCRT)" /I"$(OSIncludesUM)" /I"$(OSIncludesShared)" $(SRC_MAIN) $(SRC) /link /LIBPATH:"$(VCLibraries)" /LIBPATH:"$(OSLibrariesUCRT)" /LIBPATH:"$(OSLibrariesUCRTEnclave)" /LIBPATH:"$(OSLibrariesUM)" /SUBSYSTEM:CONSOLE

test:   $(SRC_TEST) $(SRC)
	@if [ ! -d $(BIN) ]; \
      then echo "mkdir -p $(BIN)"; mkdir -p $(BIN); \
    fi
	$(CC) /Fe"$(TARGET)" /Fo"$(BIN)" /Fd"$(BIN)" /ZI /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /EHsc /MTd  /I"$(VCIncludes)"   /I"$(OSIncludesUCRT)" /I"$(OSIncludesUM)" /I"$(OSIncludesShared)" /I"$(GTEST_INCLUDE)" /I"$(GMOCK_INCLUDE)" $(SRC_TEST) /link /LIBPATH:"$(VCLibraries)" /LIBPATH:"$(OSLibrariesUCRT)" /LIBPATH:"$(OSLibrariesUCRTEnclave)" /LIBPATH:"$(OSLibrariesUM)" /LIBPATH:"$(GTEST_LIB)" /LIBPATH:"$(GMOCK_LIB)"

clean:
	rm -rf bin/*
